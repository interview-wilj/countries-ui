# React UI for countries-api Java coding challenge

## [Click here to open the project in a workspace on Gitpod.io](https://gitpod.io/#https://gitlab.com/interview-wilj/countries-ui)

![Gitpod.io workspace example](docs/gitpod-screenshot.png)

Gitpod will automatically build and run the database, backend, and frontend. Open ports will be mapped to hostnames based on the workspace hostname, and Gitpod will attempt to open the React app in a new browser window.

To allow Gitpod popups in Chrome based browsers:

![Browser popup settings](docs/allow-popups.png)

--- 

## Local development instructions

### Running the backend

The backend is a Spring Boot application and PostgreSQL database.

Start the database first.  It will run automatically populate the database from SQL scripts.

```shell
cd countries-api
docker-compose -f postgresql.yml up
```

Next, build and run the Spring Boot application
```shell
cd countries-api

# build, test, and generate docs
./mvnw clean install spring-boot:repackage
 
# run the API
./mvnw spring-boot:run

```

Once the Spring Boot application is listening on port 8080, the React application can be launched.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
