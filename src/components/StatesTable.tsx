import { observer } from "mobx-react-lite";

import { useMst } from "../models/Root";

import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import React, { useCallback, useEffect } from "react";
import { States } from "../generated";

export const StatesTable = observer(() => {
  const {
    countries: {
      states: {
        table: { load, data, onPaginationChange, pageIndex, pageSize },
        editor: { edit, countryCode, stateCode },
      },
    },
  } = useMst();

  useEffect(() => {
    load();
  }, [load]);

  const columns = React.useMemo<ColumnDef<States>[]>(
    () => [
      {
        header: () => "Country",
        accessorKey: "countryCode",
        cell: (info) => info.getValue(),
        footer: (props) => props.column.id,
      },
      {
        header: "State",
        accessorKey: "name",
        cell: (info) => info.getValue(),
        footer: (props) => props.column.id,
      },
      {
        header: "Code",
        accessorKey: "code",
        cell: (info) => info.getValue(),
        footer: (props) => props.column.id,
      },
      {
        header: "Population",
        accessorKey: "population",
        cell: (info) => (
          <div style={{ textAlign: "right" }}>
            {(info.getValue() as any).toLocaleString("en-US")}
          </div>
        ),
        footer: (props) => props.column.id,
      },
    ],

    []
  );

  const onRowClick: React.MouseEventHandler<HTMLTableRowElement> = useCallback(
    (event) => {
      const { country, state, name, population } = (event.currentTarget as any)
        .dataset;
      edit({
        countryCode: country,
        stateCode: state,
        stateName: name,
        population: parseInt(population),
      });
    },
    [edit]
  );

  const defaultData = React.useMemo(() => [], []);

  const table = useReactTable({
    data: data?.content ?? defaultData,
    columns,
    pageCount: data?.totalPages ?? -1,
    state: {
      pagination: {
        pageIndex,
        pageSize,
      },
    },
    onPaginationChange: onPaginationChange,
    getCoreRowModel: getCoreRowModel(),
    manualPagination: true,
    debugTable: true,
  });

  return (
    <section className="subpanel">
      <h3>All states</h3>
      <div className="p-2">
        <div className="h-2" />
        <table>
          <thead>
            {table.getHeaderGroups().map((headerGroup) =>
              headerGroup.depth === 0 ? (
                <tr key={headerGroup.id}>
                  {headerGroup.headers.map((header) => {
                    return (
                      <th key={header.id} colSpan={header.colSpan}>
                        {header.isPlaceholder ? null : (
                          <div>
                            {flexRender(
                              header.column.columnDef.header,
                              header.getContext()
                            )}
                          </div>
                        )}
                      </th>
                    );
                  })}
                </tr>
              ) : null
            )}
          </thead>
          <tbody>
            {table.getRowModel().rows.map((row, index) => {
              const trClasses = [];
              if (index % 2 === 0) {
                trClasses.push("alternate");
              }
              if (
                row.original.countryCode === countryCode &&
                row.original.code === stateCode
              ) {
                trClasses.push("selected");
              }
              return (
                <tr
                  key={row.id}
                  onClick={onRowClick}
                  data-country={row.original.countryCode}
                  data-state={row.original.code}
                  data-name={row.original.name}
                  data-population={row.original.population}
                  className={trClasses.join(" ")}
                >
                  {row.getVisibleCells().map((cell) => {
                    return (
                      <td key={cell.id}>
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="h-2" />
        <div className="flex items-center gap-2">
          <button
            className="border rounded p-1"
            onClick={() => table.setPageIndex(0)}
            disabled={!table.getCanPreviousPage()}
          >
            {"<<"}
          </button>
          <button
            className="border rounded p-1"
            onClick={() => table.previousPage()}
            disabled={!table.getCanPreviousPage()}
          >
            {"<"}
          </button>
          <button
            className="border rounded p-1"
            onClick={() => table.nextPage()}
            disabled={!table.getCanNextPage()}
          >
            {">"}
          </button>
          <button
            className="border rounded p-1"
            onClick={() => table.setPageIndex(table.getPageCount() - 1)}
            disabled={!table.getCanNextPage()}
          >
            {">>"}
          </button>
          <span className="flex items-center gap-1">
            <div>Page</div>
            <strong>
              {table.getState().pagination.pageIndex + 1} of{" "}
              {table.getPageCount()}
            </strong>
          </span>
          <span className="flex items-center gap-1">
            | Go to page:
            <input
              type="number"
              defaultValue={table.getState().pagination.pageIndex + 1}
              onChange={(e) => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                table.setPageIndex(page);
              }}
              className="border p-1 rounded w-16"
            />
          </span>
        </div>
        <div className="flex items-center gap-2 mt-2">
          <select
            className="inline"
            value={table.getState().pagination.pageSize}
            onChange={(e) => {
              table.setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize} records per page
              </option>
            ))}
          </select>
        </div>
      </div>
    </section>
  );
});
