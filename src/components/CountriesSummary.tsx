import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Currencies } from "../components/Currencies";
import { Populations } from "../components/Populations";
import { useMst } from "../models/Root";

export const CountriesSummary = observer(() => {
  const { countries } = useMst();
  useEffect(() => countries.load(), [countries]);

  return (
    <>
      {countries.currencies && <Currencies currencies={countries.currencies} />}
      {countries.populations && (
        <Populations populations={countries.populations} />
      )}
    </>
  );
});
