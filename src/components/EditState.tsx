import { observer } from "mobx-react-lite";
import { useCallback, useRef } from "react";

import { useMst } from "../models/Root";

const cancelSubmit = (e: any) => e.preventDefault && e.preventDefault();

export const EditState = observer(() => {
  const {
    countries: {
      codes,
      states: {
        editor: {
          countryCode,
          stateCode,
          stateName,
          population,
          onInputChange,
          save,
          errors,
        },
      },
    },
  } = useMst();
  const formRef = useRef<HTMLFormElement>(null);
  const trySave = useCallback(() => {
    if (formRef.current?.checkValidity()) {
      save();
    }
  }, [save, formRef]);

  const errorMessage = useCallback(
    (field: string) => {
      const message = errors?.errors[field]?.message;
      if (message) {
        return <div className="formError">{message}</div>;
      }
    },
    [errors]
  );

  return (
    <section className="subpanel">
      <h3>Edit state</h3>
      <fieldset>
        <form ref={formRef} onSubmit={cancelSubmit}>
          <div className="mb-6">
            <label htmlFor="countryCode">Country code</label>
            <select
              name="countryCode"
              id="countryCode"
              value={countryCode}
              onChange={onInputChange}
              required
            >
              <option value="">Select country code</option>
              {codes.map((code) => (
                <option key={code} value={code}>
                  {code}
                </option>
              ))}
            </select>
            {errorMessage("countryCode")}
          </div>
          <div className="mb-6">
            <label htmlFor="stateCode">State code</label>
            <input
              name="stateCode"
              id="stateCode"
              value={stateCode}
              onChange={onInputChange}
              type="text"
              required
              minLength={2}
              maxLength={3}
            />
            {errorMessage("stateCode")}
          </div>
          <div className="mb-6">
            <label htmlFor="stateName">State name</label>
            <input
              name="stateName"
              id="stateName"
              value={stateName}
              onChange={onInputChange}
              type="text"
              required
              minLength={2}
              maxLength={100}
            />
            {errorMessage("stateName")}
          </div>
          <div className="mb-6">
            <label htmlFor="population">Population</label>
            <input
              name="population"
              id="population"
              value={population}
              onChange={onInputChange}
              type="number"
              required
              max="2147483647"
            />
            {errorMessage("population")}
          </div>
          <div className="mb-6">
            <button onClick={trySave}>Save</button>
          </div>
        </form>
      </fieldset>
    </section>
  );
});
