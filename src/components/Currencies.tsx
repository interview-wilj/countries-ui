import { CurrencyMap } from "../models/Countries";

export const Currencies: React.FC<{ currencies: CurrencyMap }> = ({
  currencies,
}) => (
  <section className="subpanel">
    <h3>Currencies</h3>
    <ul>
      {Object.entries(currencies).map(([key, value]) => (
        <li key={key}>
          {key}: {value}
        </li>
      ))}
    </ul>
  </section>
);
