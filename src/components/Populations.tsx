import { PopulationMap } from "../models/Countries";

export const Populations: React.FC<{ populations: PopulationMap }> = ({
  populations,
}) => (
  <section className="subpanel">
    <h3>Populations</h3>
    <ul>
      {Object.entries(populations).map(([key, value]) => (
        <li key={key}>
          {key}: {value}
        </li>
      ))}
    </ul>
  </section>
);
