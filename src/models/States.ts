import { types } from "mobx-state-tree";
import { EditState } from "./EditState";
import { StatesTable } from "./StatesTable";

export const States = types.model({
  editor: EditState,
  table: StatesTable,
});
