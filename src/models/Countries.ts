import { types } from "mobx-state-tree";
import { ApiControllerService } from "../generated";
import { primitiveObject } from "./Primitives";
import { States } from "./States";

export type CurrencyMap = Record<string, string>;
export type PopulationMap = Record<string, number>;

export const Countries = types
  .model({
    currencies: types.maybe(primitiveObject<CurrencyMap>("CurrencyMap")),
    populations: types.maybe(primitiveObject<PopulationMap>("PopulationMap")),
    codes: types.optional(types.array(types.string), []),
    states: States,
  })
  .actions((self) => ({
    setCurrencies(currencies: any) {
      self.currencies = currencies;
      self.codes.replace(Object.keys(currencies).sort());
    },
    setPopulations(populations: any) {
      self.populations = populations;
    },
  }))
  .actions((self) => ({
    load() {
      ApiControllerService.getAllCountryCurrencies().then(self.setCurrencies);
      ApiControllerService.getAllCountryPopulations().then(self.setPopulations);
    },
  }));
