import { PaginationState, Updater } from "@tanstack/react-table";
import { types } from "mobx-state-tree";
import { ApiControllerService, PageStates } from "../generated";
import { primitiveObject } from "./Primitives";

export const StatesTable = types
  .model({
    pageIndex: types.optional(types.number, 0),
    pageSize: types.optional(types.number, 10),
    data: primitiveObject<PageStates>("StatesTableData"),
    result: types.maybe(types.boolean),
  })
  .actions((self) => ({
    setData(data: PageStates) {
      self.data = data;
    },
  }))
  .actions((self) => ({
    load() {
      ApiControllerService.allStates(self.pageIndex, self.pageSize).then(
        self.setData
      );
    },
  }))
  .actions((self) => ({
    setPagination(pagination: PaginationState) {
      self.pageIndex = pagination.pageIndex;
      self.pageSize = pagination.pageSize;
      self.load();
    },
  }))
  .actions((self) => ({
    onPaginationChange(updater: Updater<PaginationState>) {
      let pagination: PaginationState;
      if (typeof updater === "function") {
        pagination = updater({
          pageIndex: self.pageIndex,
          pageSize: self.pageSize,
        });
      } else {
        pagination = updater;
      }
      self.setPagination(pagination);
    },
  }));
