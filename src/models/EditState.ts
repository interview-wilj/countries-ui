import { getRoot, types } from "mobx-state-tree";
import { ChangeEvent } from "react";
import { ApiControllerService, State } from "../generated";
import { asFormErrors, FormErrors } from "../types/errors";
import { primitiveObject } from "./Primitives";
import { RootInstance } from "./Root";

export const EditState = types
  .model({
    countryCode: types.optional(types.string, ""),
    stateCode: types.optional(types.string, ""),
    stateName: types.optional(types.string, ""),
    population: types.optional(types.number, 0),
    dirty: types.optional(types.boolean, false),
    result: types.maybe(primitiveObject<State>("EditStateResult")),
    errors: types.maybe(primitiveObject<FormErrors>("EditStateErrors")),
  })
  .actions((self) => ({
    setResult(result: State) {
      self.dirty = false;
      self.result = result;
    },
    setErrors(errors?: FormErrors) {
      self.errors = errors;
    },
    setCountryCode(value?: string) {
      self.countryCode = value || "";
    },
    setStateCode(value?: string) {
      self.stateCode = value || "";
    },
    setStateName(value?: string) {
      self.stateName = value || "";
    },
    setPopulation(value?: string | number) {
      if (typeof value === "number") {
        self.population = value;
      } else {
        self.population = parseInt(value?.trim() || "0");
      }
    },
  }))
  .actions((self) => ({
    edit(state: State) {
      self.setCountryCode(state.countryCode);
      self.setStateCode(state.stateCode);
      self.setStateName(state.stateName);
      self.setPopulation(state.population);
      self.result = null;
      self.dirty = false;
      self.errors = null;
    },
  }))
  .actions((self) => ({
    save() {
      ApiControllerService.addState(self.countryCode, self.stateCode, {
        countryCode: self.countryCode,
        stateCode: self.stateCode,
        stateName: self.stateName,
        population: self.population,
      })
        .then((result) => {
          self.edit(result);
          const { countries } = getRoot<RootInstance>(self);
          countries.load();
          countries.states.table.load();
        })
        .catch((e) => {
          self.setErrors(asFormErrors(e));
        });
    },
  }))
  .actions((self) => ({
    onInputChange(event: ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
      const {
        currentTarget: { name, value },
      } = event;
      switch (name) {
        case "countryCode":
          self.setCountryCode(value);
          break;
        case "stateCode":
          self.setStateCode(value);
          break;
        case "stateName":
          self.setStateName(value);
          break;
        case "population":
          self.setPopulation(value);
          break;
      }
      self.dirty = true;
    },
  }));
