import { Instance, types } from "mobx-state-tree";
import { createContext, useContext } from "react";
import { OpenAPI } from "../generated";
import { Countries } from "./Countries";

// override generated default base URL which points to localhost
OpenAPI.BASE = "";

const RootModel = types.model({
  countries: Countries,
});

let initialState = RootModel.create({
  countries: { states: { editor: {}, validator: {}, table: {} } },
});

export const rootStore = initialState;

export type RootInstance = Instance<typeof RootModel>;
const RootStoreContext = createContext<null | RootInstance>(null);

export const Provider = RootStoreContext.Provider;
export function useMst() {
  const store = useContext(RootStoreContext);
  if (store === null) {
    throw new Error("Store cannot be null, please add a context provider");
  }
  return store;
}

// keep a global reference to the root store for easy debugging from devtools console
window && ((window as any).___COUNTRIES = { rootStore });
