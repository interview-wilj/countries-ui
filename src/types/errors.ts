import { ApiError } from "../generated";

export interface ServiceErrors {
  timestamp: Date;
  status: number;
  error: string;
  trace: string;
  message: string;
  errors: ServiceError[];
  path: string;
}

export interface ServiceError {
  codes: string[];
  arguments: Array<ServiceArgument | number>;
  defaultMessage: string;
  objectName: string;
  field: string;
  rejectedValue: number;
  bindingFailure: boolean;
  code: string;
}

export interface ServiceArgument {
  codes: string[];
  arguments: null;
  defaultMessage: string;
  code: string;
}

export function asServiceErrors(x: any) {
  if (
    "timestamp" in x &&
    "status" in x &&
    "error" in x &&
    "trace" in x &&
    "message" in x &&
    "errors" in x &&
    "path" in x
  ) {
    return x as ServiceErrors;
  } else {
    return undefined;
  }
}

export type FormError = {
  field: string;
  message: string;
};
export type FormErrors = {
  message: string;
  errors: { [id: string]: FormError };
};

export function asApiError(x: any) {
  if (x && "url" in x && "body" in x) {
    return x as ApiError;
  }
  return undefined;
}

export function asFormErrors(x: any): FormErrors {
  const apiError = asApiError(x);
  const serviceErrors = asServiceErrors(apiError?.body || x);
  if (serviceErrors) {
    const { message, errors } = serviceErrors;
    const result: FormErrors = {
      message,
      errors: {},
    };
    errors.forEach(({ field, defaultMessage }) => {
      result.errors[field] = { field, message: defaultMessage };
    });
    return result;
  } else {
    // does it have any kind of message?
    const key = ["message", "defaultMessage", "error", "errors"].find(
      (key) => key in x
    );
    const message = key ? `${x[key]}` : JSON.stringify(x);
    return { message, errors: {} };
  }
}
