/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PageStates } from '../models/PageStates';
import type { State } from '../models/State';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class ApiControllerService {

    /**
     * @param countryCode
     * @param stateCode
     * @param requestBody
     * @returns State OK
     * @throws ApiError
     */
    public static addState(
        countryCode: string,
        stateCode: string,
        requestBody: State,
    ): CancelablePromise<State> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/api/countries/{countryCode}/states/{stateCode}',
            path: {
                'countryCode': countryCode,
                'stateCode': stateCode,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param countryCode
     * @param stateCode
     * @returns boolean OK
     * @throws ApiError
     */
    public static validateState(
        countryCode: string,
        stateCode: string,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/countries/{countryCode}/states/{stateCode}/validate',
            path: {
                'countryCode': countryCode,
                'stateCode': stateCode,
            },
        });
    }

    /**
     * @param pageNo
     * @param pageSize
     * @param sortBy
     * @param sortDir
     * @returns PageStates OK
     * @throws ApiError
     */
    public static allStates(
        pageNo?: number,
        pageSize: number = 10,
        sortBy: string = 'name',
        sortDir: string = 'asc',
    ): CancelablePromise<PageStates> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/countries/states',
            query: {
                'pageNo': pageNo,
                'pageSize': pageSize,
                'sortBy': sortBy,
                'sortDir': sortDir,
            },
        });
    }

    /**
     * @returns any OK
     * @throws ApiError
     */
    public static getAllCountryPopulations(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/countries/populations',
        });
    }

    /**
     * @returns any OK
     * @throws ApiError
     */
    public static getAllCountryCurrencies(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/countries/currencies',
        });
    }

}
