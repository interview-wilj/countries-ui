/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type State = {
    countryCode: string;
    stateCode: string;
    stateName: string;
    population: number;
};

