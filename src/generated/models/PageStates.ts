/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PageableObject } from './PageableObject';
import type { Sort } from './Sort';
import type { States } from './States';

export type PageStates = {
    totalPages?: number;
    totalElements?: number;
    size?: number;
    content?: Array<States>;
    number?: number;
    sort?: Sort;
    pageable?: PageableObject;
    first?: boolean;
    last?: boolean;
    numberOfElements?: number;
    empty?: boolean;
};

