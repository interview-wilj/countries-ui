/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type States = {
    countryCode?: string;
    code?: string;
    name?: string;
    population?: number;
};

