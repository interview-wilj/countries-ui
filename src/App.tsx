import "./App.css";
import { CountriesSummary } from "./components/CountriesSummary";
import { EditState } from "./components/EditState";
import { StatesTable } from "./components/StatesTable";
import { Provider, rootStore } from "./models/Root";

function App() {
  return (
    <Provider value={rootStore}>
      <h1>Countries UI</h1>
      <div className="grid grid-cols-4 gap-2">
        <section className="panel col-span-4 md:col-span-1">
          <h2>Summary</h2>
          <CountriesSummary />
        </section>
        <section className="panel col-span-4 md:col-span-3">
          <h2>States</h2>
          <EditState />
          <StatesTable />
        </section>
      </div>
    </Provider>
  );
}

export default App;
