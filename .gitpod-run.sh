#!/usr/bin/env bash
set -euo pipefail

echo "DANGEROUSLY_DISABLE_HOST_CHECK=true" > .env.development.local

# for some reason the SaaS version of Gitpod breaks node_modules when using prebuilds
rm -rf node_modules

npm install

echo "Waiting for Spring Boot API to start..."
until $(curl --output /dev/null --silent --head --fail http://localhost:8080/v3/api-docs/); do
    printf '.'
    sleep 5
done

echo "Starting React application..."

npm run start